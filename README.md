


How to install
===============

Symlink dotfiles with gnu stow the following way.

```
/home/user/dotfiles> stow <tool> 
```

System tools
=============

```
apt install build-essential cmake libssl-dev fontconfig libfontconfig1-dev git 
snap install nvim --classic
```

Rust dependencies
===================

Run rustup from https://rustup.rs/

```
cargo install alacritty nu starship mprocs ytop bandwhich rust-analyzer cargo-generate cargo-make exa fd-find procs du-dust ripgrep tokei hyperfine tealdeer zoxide git-delta tpnote sd mprocs bat mdbook irust
```


Other tools
============
utm 
visual studio code
lapce





