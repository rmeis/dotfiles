
# Summary

Scripts to install all my dependencies and tools i need on my development machines.
Instead of using ansible or other infrastructure as code tools, i decided to stick with rust tooling to achieve the same. 
Why? Because i really enjoy the rust ecosystem and want to get more familiar with it.

Running 
'''shell 
sh setup.sh
'''
will first install rustup and cargo make. Afterwards all dependencies listed under tasks.basics in the Makefile.toml will 
be installed via cargo make taskrunner.
